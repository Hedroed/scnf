from __future__ import annotations

import datetime
import random
import string
from typing import AbstractSet, AsyncGenerator, Iterator, Protocol, Sequence

import dateutil.tz
import edgedb


def get_english_timedelta_description(delta: datetime.timedelta) -> str:
    days = delta.days + delta.seconds / 3600 / 24
    hours = delta.seconds / 3600
    minutes = delta.seconds % 3600 / 60
    seconds = delta.seconds % 3600 % 60

    if days > 0.5:
        if 0.9 < days < 1:
            days = 0.9
        unit = "day" if days == 1 else "days"
        return f"{days:.0f} {unit}" if days == int(days) else f"{days:.1f} {unit}"
    elif hours > 0.5:
        if 0.9 < hours < 1:
            hours = 0.9
        unit = "hour" if hours == 1 else "hours"
        if hours == int(hours):
            return f"{hours:.0f} {unit}"
        return f"{hours:.1f} {unit}"
    elif minutes > 0.5:
        if 0.9 < minutes < 1:
            minutes = 0.9
        unit = "minute" if minutes == 1 else "minutes"
        if minutes == int(minutes):
            return f"{minutes:.0f} {unit}"
        return f"{minutes:.1f} {unit}"
    else:
        unit = "second" if seconds == 1 else "seconds"
        return f"{seconds} {unit}"


def get_english_dt_description_from_now(ts: datetime.datetime) -> str:
    delta = nowtz() - ts
    return get_english_timedelta_description(delta)


def nowtz() -> datetime.datetime:
    return datetime.datetime.now(tz=dateutil.tz.tzlocal())  # type: ignore


def icon_class(tag: str) -> str:
    i_class = _icon_classes.get(tag.lower(), "")
    if not i_class and tag.endswith("s"):
        i_class = _icon_classes.get(tag.lower()[:-1], "")
    return "uil-" + (i_class or "angle-right")


def make_tags_query(needle: str, haystack: AbstractSet[str]) -> str:
    """Return a URL query string for tags.

    `haystack` is the previous set of tags. If `needle` was in it, remove it.
    If not, add it.
    """
    new_tags = set(haystack)
    if needle in haystack:
        new_tags.remove(needle)
    else:
        new_tags.add(needle)
    return "&".join(f"t={tag}" for tag in new_tags)


class ContentItem(Protocol):
    ts: datetime.datetime
    name: str
    title: str | None
    text: str
    tags: list[str]


async def query_content(
    db: edgedb.AsyncIOPool, tags: AbstractSet[str] = frozenset()
) -> Sequence[ContentItem]:
    query = """
        WITH MODULE scnf
        SELECT Content {
            ts := .public_since ?? datetime_current(),
            name,
            title,
            text := [IS Note].text,
            tags
        }
    """
    ordering = " ORDER BY .ts DESC; "
    if tags:
        filtering = " FILTER all(array_unpack(<array<util::Tag>>$tags) IN .tags) "
        content = await db.query(query + filtering + ordering, tags=list(tags))
    else:
        content = await db.query(query + ordering)
    return content


async def drop_test_data(db: edgedb.AsyncIOPool) -> AsyncGenerator[str, None]:
    yield "Dropping all data from database\n"
    typenames = await db.query(
        """
        SELECT schema::ObjectType { name }
        FILTER .name LIKE 'scnf::%'
        AND .is_abstract = false;
        """
    )
    retry = True
    while retry:
        retry = False
        for typeobj in typenames:
            typename = typeobj.name
            yield f"Deleting {typename} objects\n"
            try:
                await db.query(f"DELETE {typename}")
            except edgedb.ConstraintViolationError:
                retry = True

    yield "Done dropping test data\n"


async def make_test_data(db: edgedb.AsyncIOPool) -> AsyncGenerator[str, None]:
    yield "Populating test data\n"
    usernames = ["ambv", "yury", "elprans"]
    tags = [
        "articles",
        "bookmarks",
        "fiction",
        "guitar",
        "inspirations",
        "journal",
        "learning",
        "philosophy",
        "python",
        "quotes",
    ]
    seconds_in_3_years = 60 * 60 * 24 * 365 * 3

    for user in usernames:
        yield f"Inserting user {user}\n"
        try:
            await db.query(
                """
                WITH MODULE scnf
                INSERT User { name := <util::Slug>$name };
                """,
                name=user,
            )
        except edgedb.ExecutionError as ee:
            yield f"ERROR: {str(ee)}\n"

    content_count = 40
    for i in range(1, content_count + 1):
        yield f"{i}/{content_count}: inserting note\n"
        text = " ".join(lorem_ipsum(random.randint(3, 20)))
        content_title = None
        if random.random() > 0.8:
            content_title = " ".join(lorem_ipsum(5))
        content_name = "-".join(random_string(8) for _ in range(3)).lower()
        seconds = random.randint(0, seconds_in_3_years)
        seconds = random.randint(0, seconds)
        seconds = random.randint(0, seconds)
        public_toss = random.random()
        public_since: datetime.datetime | None
        public_until: datetime.datetime | None
        if public_toss < 0.25:
            public_since = nowtz() - datetime.timedelta(seconds=seconds)
            public_until = nowtz()
        elif public_toss < 0.5:
            public_since = nowtz() + datetime.timedelta(seconds=seconds)
            public_until = None
        elif public_toss >= 0.5:
            public_since = None
            public_until = None
        try:
            await db.query(
                """
                WITH MODULE scnf
                INSERT Note {
                    title := <OPTIONAL str>$title,
                    name := <util::Slug>$name,
                    text := <str>$text,
                    tags := array_unpack(<array<util::Tag>>$tags),
                    public_since := <OPTIONAL datetime>$public_since,
                    public_until := <OPTIONAL datetime>$public_until,
                    deleted := <bool>$deleted
                }
                """,
                title=content_title,
                name=content_name,
                text=text,
                tags=random.sample(tags, random.randint(0, 3)),
                public_since=public_since,
                public_until=public_until,
                deleted=False if random.random() > 0.1 else True,
            )
        except edgedb.ExecutionError as ee:
            yield f"ERROR: {str(ee)}\n"

    yield "Done making test data\n"
