from rich import print
import requests
import json
import time
import uuid


cookies = {
    # '__Secure-cms-account': 'eyJpc0xvZ2dlZEluIjpmYWxzZX0=',
}

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:99.0) Gecko/20100101 Firefox/99.0',
    'x-bff-key': 'ah1MPO-izehIHD-QZZ9y88n-kku876',
}

montparnasse = {
    'id': 'RESARAIL_STA_FRPMO',
    'label': 'Paris Gare Montparnasse',
}

rennes = {
    'id': 'RESARAIL_STA_FRRNS',
    'label': 'Rennes',
}

rospo = {
    'id': 'RESARAIL_STA_FRRSP',
    'label': 'Rosporden',
}

massy = {
    'id': 'RESARAIL_STA_FRDJU',
    'label': 'Paris Massy',
}

vannes = {
    'id': 'RESARAIL_STA_FRVNE',
    'label': 'Vannes'
}

guillestre = {
    'id': 'RESARAIL_STA_FRIGZ',
    'label': 'Mont-Dauphin - Guillestre'
}

nantes = {
    'id': 'RESARAIL_STA_FRNTE',
    'label': 'Nantes',
}

poitier = {
    'id': 'RESARAIL_STA_FRPIS',
    'label': 'Poitiers'
}


json_data = {
    'schedule': {
        'outward': {
            'date': '2022-10-23T16:00:00.000Z',
            'arrivalAt': False,
        },
    },
    'mainJourney': {
        'origin': nantes,
        'destination': montparnasse,
    },
    'passengers': [
        {
            'id': '0',
            'discountCards': [
                {
                    'code': 'YOUNG_PASS',
                    'label': '',
                    'number': '29090104000000000',
                },
            ],
            'typology': 'YOUNG',
            'withoutSeatAssignment': False,
            'age': 25,
            'dateOfBirth': '1997-01-01',
            'displayName': '',
        },
    ],
    'pets': [],
    'itineraryId': str(uuid.uuid4()),
    'forceDisplayResults': True,
    'trainExpected': True,
    'wishBike': False,
    'strictMode': False,
}

response = requests.post('https://www.sncf-connect.com/bff/api/v1/itineraries', cookies=cookies, headers=headers, json=json_data)
try:
    response.raise_for_status()
except requests.HTTPError:
    print(response.text)

ret = response.json()

try:

    root = ret['longDistance']['proposals']

    prices = root['pricesRange']
    r = {
        'range': [
            {
                'min': item['priceMin']['priceLabel'],
                'max': item['priceMax']['priceLabel'],
                'type': item['transporter']['description'],
            }
            for item in prices['pricesRangeItems']
        ],
    }
    print(r)

    trains = root['proposals']
    for train in trains:
        bookable = train['status']['isBookable']
        travel = train['travelId']
        duration = train['durationLabel']
        bestPrice = train.get('bestPriceLabel', None)
        transporter = train['transporterDescription']
        if 'firstComfortClassOffers' in train:
            first_prices = [
                {
                    'price': price['priceLabel'],
                    'remains': [msg['message'] for msg in price.get('messages', []) if 'Plus que' in msg['message']],
                }
                for price in train['firstComfortClassOffers']['offers']
            ]
        else:
            first_prices = []
        second_prices = [
            {
                'price': price['priceLabel'],
                'remains': [msg['message'] for msg in price.get('messages', []) if 'Plus que' in msg['message']],
            }
            for price in train['secondComfortClassOffers']['offers']
        ]

        segments = [
            {
                'from': f"{seg['departure']['originStationLabel']} - {seg['departure']['timeLabel']}",
                'to': f"{seg['arrival']['destinationStationLabel']} - {seg['arrival']['timeLabel']}",
                'duration': seg['durationLabel'],
                'transporter': seg['transporter']['description'],
                'transit': seg.get('transitDuration', None),
            }
            for seg in train['timeline']['segments']
        ]

        d = {
            'bookable': bookable,
            'travel': travel,
            'bestPrice': bestPrice,
            'duration': duration,
            'transporter': transporter,
            'first_prices': first_prices,
            'second_prices': second_prices,
            'segments': segments,
        }

        print(d)
except KeyError as err:
    print(ret)
    print(err)
finally:
    with open(f'out/{time.time():0f}_ret.json', 'w') as fp:
        json.dump(ret, fp)
