import requests
import json
from rich import print
import logging

log = logging.getLogger(__name__)


COOKIES = {
    # '__Secure-cms-account': 'eyJpc0xvZ2dlZEluIjpmYWxzZX0=',
}

HEADERS = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:99.0) Gecko/20100101 Firefox/99.0',
    # Already added when you pass json=
    # 'Content-Type': 'application/json',
    'x-bff-key': 'ah1MPO-izehIHD-QZZ9y88n-kku876',
    # 'Cookie': '__Secure-cms-account=eyJpc0xvZ2dlZEluIjpmYWxzZX0=',
}

def search_station(name: str):
    json_data = {
        'searchTerm': name,
        'keepStationsOnly': False,
    }

    log.debug('querying data %s', json_data)
    response = requests.post('https://www.sncf-connect.com/bff/api/v1/autocomplete', cookies=COOKIES, headers=HEADERS, json=json_data)
    response.raise_for_status()

    ret = response.json()


    places = ret['places']['transportPlaces']
    for place in places:
        if place['type']['label'] != 'Gare':
            continue

        if all(code['type'] != 'RESARAIL' for code in place['codes']):
            continue

        id_ = place['id']
        label = place['label']

        d = {
            'id': id_,
            'label': label,
        }

        print(d)
