import argparse
import logging
import sys
from rich import print


from train.req import search_station as req_search_station


def search_station(args_name):
    req_search_station(' '.join(args_name))


def get_train(args):
    print(args)


def args_parser() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='I like trains')
    parser.add_argument('-v', '--verbose', action='count', help='Increase verbosity')
    parser.add_argument('--version', action='store_true', help='Show version')

    subparsers = parser.add_subparsers()

    parser_station = subparsers.add_parser('station')
    parser_station.add_argument('name', type=str, nargs='+', help='name of the station to search')
    parser_station.set_defaults(func=search_station)

    parser_get = subparsers.add_parser('get')
    parser_get.add_argument('from', type=str, help='departure station')
    parser_get.add_argument('to', type=str, help='arrival station')
    parser_get.add_argument('at', type=str, help='date and time of the travel, ISO8601 2022-10-07T12:45:00')
    parser_get.add_argument('-i', action='store_true', help='invert departure and arrival position')
    parser_get.set_defaults(func=get_train)

    return parser.parse_args()


args = args_parser()

logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO)

sys.exit(
    args.func(args.name)
)
