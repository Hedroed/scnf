from rich import print
import requests
import json
import time


cookies = {
    # '__Secure-cms-account': 'eyJpc0xvZ2dlZEluIjpmYWxzZX0=',
}

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:99.0) Gecko/20100101 Firefox/99.0',
    # Already added when you pass json=
    # 'Content-Type': 'application/json',
    'x-bff-key': 'ah1MPO-izehIHD-QZZ9y88n-kku876',
    # 'Cookie': '__Secure-cms-account=eyJpc0xvZ2dlZEluIjpmYWxzZX0=',
}

json_data = {
    'searchTerm': 'paris',
    'keepStationsOnly': False,
}

response = requests.post('https://www.sncf-connect.com/bff/api/v1/autocomplete', cookies=cookies, headers=headers, json=json_data)
response.raise_for_status()

ret = response.json()

try:

    places = ret['places']['transportPlaces']
    for place in places:
        if place['type']['label'] != 'Gare':
            continue

        id_ = place['id']
        label = place['label']
        description = place['description']

        d = {
            'id': id_,
            'label': label,
            'description': description,
        }

        print(d)

        print(json.dumps({'id': id_, 'label': label, }, indent=4))
except KeyError:
    print(ret)
    raise
finally:
    with open(f'out/{time.time():0f}_autocomplete.json', 'w', encoding='utf-8') as fp:
        json.dump(ret, fp)
