module util {
    scalar type Slug extending str {
        constraint regexp(r'^[a-zA-Z0-9][a-zA-Z0-9./- _]+$');
    };
};
