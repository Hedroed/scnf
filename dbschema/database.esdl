module scnf {
    type Alert extending
        Named,
        Dated,
        Scheduled,
        Notifiable;

    abstract type Named {
        required property name -> util::Slug {
            constraint exclusive;
        };
        index on (__subject__.name);
    };

    abstract type Dated {
        property created_at -> datetime {
            readonly := true;
            default := datetime_current();
        }
    };

    type Train {
        required property remains -> int64;
        required property time -> datetime;
        required property code -> str;
        required property duration -> duration;
        required property transporter -> str;
        optional property bestPrice -> float64;
    };

    type State {
        required property id -> str;
        required property label -> str;
        optional property description -> str;
    };

    type PriceRange {
        required property min -> float64;
        required property max -> float64;
        constraint expression on (
            .min <= .max
        );
    }

    abstract type Scheduled {
        required property date -> datetime;
        link prices -> PriceRange;
        link departure -> Station;
        link arrival -> Station;
        multi link trains -> Train {
            # ensures a one-to-many relationship
            constraint exclusive;
        }
    };

    abstract type Notifiable {
        required property notifier -> str;
    };
};
